API запросы синхронизации
=========================

Все перечисленные ниже АПИ запросы работают одинаково в каждом из
[контекстов][context], если в описании запроса не сказано обратное.

В описнании запросов используется подстановка `<context-url>` для
базового адреса нужного контекста. Значение этого адреса см. в описании
соответствующего [контекста][context].


Основное
--------

*   [Запрос снапшота](snapshot.md)
*   [Запрос разницы](diff.md)
*   [Отправка изменений](submit.md)
*   [Запрос тикета](ticket.md)

Загрузка файлов
---------------

*   [Подготовка загрузки файлов](uploader.md)
*   [Загрузка файла](upload.md)


[context]: ../02-context.md
