Синхронизация
=============

Содержание
----------

*   [Введение](01-intro.md)
*   [Контекст данных](02-context.md)
    *   [Публичные](context/global.md)
    *   [Личные](context/user.md)
    *   [Командные](context/team.md)
*   [Сценарии сеансов синхронизации](03-scenarios.md)
*   [Ход сеанса синхронизации](04-workflow.md)
*   [Загрузка файлов](10-uploads.md)
*   [API](api/README.md)
    *   [Запрос снапшота](api/snapshot.md)
    *   [Запрос разницы](api/diff.md)
    *   [Отправка изменений](api/submit.md)
    *   [Запрос тикета](api/ticket.md)
    *   [Подготовка загрузки файлов](api/uploader.md)
    *   [Загрузка файла](api/upload.md)
*   [Ограничения](20-limitations.md)
*   [Классификация ошибок](30-errors.md)
